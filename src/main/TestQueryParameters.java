package main;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class TestQueryParameters extends HttpServlet {
	Logger infoLogger = Logger.getLogger("assignment2");
	String name = "";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	//@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException
	{
		createDirs();
		HttpSession session = request.getSession(false);
		if(session != null){
			infoLogger.info("Session in Action: ");
			//			session.invalidate();
			//			infoLogger.info("Session invalidated: ");
			this.loggedInform(request, response, true);
			return;
		}

		printLogin(response);
	}


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{	

		if(request.getParameter("registerForm") != null){
			this.registerForm(request);
		}
		if(request.getParameter("logout") != null){
			invalidateSession(request);
			this.printLogin(response);
			return;
		}

		if(request.getParameter("register") != null){
			this.infoLogger.info("Register Form");
			response.getWriter().println("<html>");
			response.getWriter().println("<form action=\"/assignment2/queryparam\" method=\"post\">");
			response.getWriter().println("Name: <input type=\"text\" name=\"name\">");
			response.getWriter().println("<br>");
			response.getWriter().println("Password: <input type=\"text\" name=\"password\">");
			response.getWriter().println("<br>");
			response.getWriter().println("email: <input type=\"text\" name=\"email\">");
			response.getWriter().println("<br>");
			response.getWriter().println("<input type=\"submit\" name = \"registerForm\" value = \"Submit\"></input>");
			response.getWriter().println("</form>");
			response.getWriter().println("</html>");
			return;

		}

		if(request.getParameter("submitquery") != null){
			this.submitQuery(request, response);
		}
		if(request.getParameter("login") != null){
			loggedInform(request, response,false);
		}
		else{
			printLogin(response);
		}
	}


	public boolean printLogin(HttpServletResponse response) throws IOException{
		response.getWriter().println("<html>");
		response.getWriter().println("<H1> Assignment 2</H1>");
		response.getWriter().println("<form action=\"/assignment2/queryparam\" method=\"post\">");
		response.getWriter().println("<input type=\"text\" name=\"username\">");
		response.getWriter().println("<input type=\"password\" name=\"password\">");	
		response.getWriter().println("<br>");
		response.getWriter().println("<input type=\"submit\" name = \"login\" value = \"Log in\"></input>");
		response.getWriter().println("<input type=\"submit\" name = \"register\" value = \"Register\"></input>");
		response.getWriter().println("</form>");
		response.getWriter().println("</html>");
		return true;
	}

	public boolean createDirs() throws IOException{
		Path p1 = Paths.get("/tmp/nel349/assignment2/");
		if (Files.notExists(p1)) {
			this.infoLogger.info("Creating directories");
			File file = new File("/tmp/nel349/assignment2/userlogins.txt");
			file.getParentFile().mkdirs();
			File file2 = new File("/tmp/nel349/assignment2/userhistory.txt");
			file.createNewFile();
			file2.createNewFile();
			return false;
		}
		return true;
	}

	public boolean authenticate(String user, String password, HttpServletRequest request) throws FileNotFoundException{
		this.infoLogger.info("Autenticate user");
		String filename = "/tmp/nel349/assignment2/userlogins.txt";
		File file = null;
		file = new File(filename);

		Scanner	sc = new Scanner(file);

		while(sc.hasNextLine()){
			String line = sc.nextLine();
			StringTokenizer ss = new StringTokenizer(line, ";");
			String f_user = ss.nextToken();
			String f_password = ss.nextToken();
			if(f_user.equals(user) && f_password.equals(password) ){
				sc.close();
				HttpSession session = request.getSession(true);
				session.setAttribute("user", f_user);
				session.setAttribute("password", f_password);
				return true;
			}
		}
		sc.close();
		return false;
	}

	public boolean loggedInform(HttpServletRequest request, HttpServletResponse response, boolean firstTime) throws FileNotFoundException, IOException{

		HttpSession l_session = request.getSession(false);
		if(l_session != null){
			name = (String)request.getSession().getAttribute("user");
		}else{
			name = request.getParameter("username"); 
		}


		String attempted_username =(l_session != null) ?(String)l_session.getAttribute("user") :request.getParameter("username");
		String attempted_password =(l_session != null) ?(String)l_session.getAttribute("password") :request.getParameter("password");
		this.infoLogger.info("Attempted username: " + attempted_username);
		this.infoLogger.info("Attempted password: ******");

		if (attempted_username!= null && attempted_password != null
				&& authenticate(attempted_username, attempted_password, request)) {
			Cookie cookie = new Cookie("logged_in", "me");
			cookie.setMaxAge(1000);
			response.addCookie(cookie);

			response.getWriter().println("<html>");
			response.getWriter().println("<br>Welcome, " + name);
			response.getWriter().println("<form action=\"/assignment2/queryparam\" method=\"post\">");
			response.getWriter().println("<input type=\"text\" name=\"type\">");
			response.getWriter().println("<input type=\"text\" name=\"project\">");
			response.getWriter().println("<input type=\"text\" name=\"year\">");
			response.getWriter().println("<input type=\"submit\" name = \"submitquery\"></input>");
			response.getWriter().println("</form>");


			response.getWriter().println("<form action=\"/assignment2/queryparam\" method=\"post\">");
			response.getWriter().println("<input type=\"submit\" value = \"Log out\" name= \"logout\"></input>");
			response.getWriter().println("</form>");
			response.getWriter().println("</html>");
			retrieveHistory(request, response);
			return true;				
		}else{
			printLogin(response);
			if(firstTime == false)
				response.getWriter().println("<br>User or password is incorrect ");
		}

		return true;
	}

	public boolean retrieveHistory(HttpServletRequest request, HttpServletResponse response) throws IOException{		
		HttpSession session = request.getSession(true);
		String user = (String) session.getAttribute("user");
		this.infoLogger.info("This is my current user: " + user);


		//Retrieve links from file
		response.getWriter().println("<html>");		
		//		String filename = getServletContext().getRealPath("/WEB-INF/userhistory.txt");
		String filename = "/tmp/nel349/assignment2/userhistory.txt";

		File file = new File(filename);	
		Scanner sc = new Scanner(file);
		while(sc.hasNextLine()){
			String line = sc.nextLine();
			StringTokenizer ss = new StringTokenizer(line, " ");
			String f_user = ss.nextToken();

			String f_url = ss.nextToken();
			if(f_user.equals(user)){
				this.infoLogger.info("retrieved from history: " + f_user);
				response.getWriter().println("<a href="+ f_url +">" + f_url +"</a>");
				response.getWriter().println("<br>");
			}
		}
		sc.close();
		response.getWriter().println("</html>");
		return true;
	}

	public boolean invalidateSession(HttpServletRequest request){
		HttpSession session = request.getSession(false);
		if(session != null){
			session.invalidate();
			return true;
		}
		return false;
	}

	public boolean submitQuery(HttpServletRequest request, HttpServletResponse response) throws IOException{
		String type = request.getParameter("type");
		String project = request.getParameter("project");
		String year = request.getParameter("year");

		if(type != null && project != null && year != null){
			String contextPath= "http://eavesdrop.openstack.org/";
			if(type.equals("irclogs") && !project.isEmpty())
				project = "%23" + project;
			String wholePath = contextPath + "/" + type + "/"+ project +"/" + year;
			response.sendRedirect(response.encodeRedirectURL(wholePath));

			//			String filename = getServletContext().getRealPath("/WEB-INF/userhistory.txt");
			String filename = "/tmp/nel349/assignment2/userhistory.txt";


			File file =new File(filename);
			PrintStream p = new PrintStream(new BufferedOutputStream(new FileOutputStream(file, true)));
			p.println(name+" "+wholePath);
			this.infoLogger.info("Doing query: " + wholePath);
			p.close();
			return true;
		}
		
		return false;
	}
	
	public boolean registerForm(HttpServletRequest request){
		try{
			String name = request.getParameter("name");
			String password = request.getParameter("password");
			String email = request.getParameter("email");

			String data = name + ";"+password + ";"+ email;
			this.infoLogger.info("Registering " +name);
			String filename = "/tmp/nel349/assignment2/userlogins.txt";
			File file =new File(filename);
			PrintStream p = new PrintStream(new BufferedOutputStream(new FileOutputStream(file, true)));
			p.println(data);
			p.close();
			this.infoLogger.info("User registerd");
		}catch(IOException e){
			e.printStackTrace();
		}
		return true;
	}
}

