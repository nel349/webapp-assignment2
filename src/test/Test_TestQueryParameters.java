package test;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import main.TestQueryParameters;


public class Test_TestQueryParameters{
	@Mock
	TestQueryParameters ex ;
	
    @Before  
    public void setup() {  
    	ex = mock(TestQueryParameters.class);
    	
    }
    
    
	@Test
	public void Test_printLogin() throws IOException  {
		HttpServletResponse response = mock(HttpServletResponse.class); 
		when(ex.printLogin(response)).thenReturn(true);		
		assertEquals(true, ex.printLogin(response));
	}

	
	@Test
	public void Test_authenticate() throws IOException  {
		HttpServletRequest request = mock(HttpServletRequest.class);       
		String user = "Norman";
		String password = "r234";
		when(ex.authenticate(user, password, request)).thenReturn(true);		
		assertEquals(true, ex.authenticate(user, password, request));
	}
	
	@Test
	public void Test_loggedInform() throws IOException  {
		HttpServletRequest request = mock(HttpServletRequest.class);       
		HttpServletResponse response = mock(HttpServletResponse.class); 
		
		when(ex.loggedInform(request, response, false)).thenReturn(true);		
		assertEquals(true, ex.loggedInform(request, response, false));
		
		when(ex.loggedInform(request, response, true)).thenReturn(false);		
		assertEquals(false, ex.loggedInform(request, response, true));

		
	}
	
	@Test
	public void retrieveHistory() throws FileNotFoundException, IOException{
		HttpServletRequest request = mock(HttpServletRequest.class);       
		HttpServletResponse response = mock(HttpServletResponse.class); 
		
		when(ex.retrieveHistory(request, response)).thenReturn(true);		
		assertEquals(true, ex.retrieveHistory(request, response));
	}
	
	@Test
	public void invalidateSession() throws IOException{
		HttpServletRequest request = mock(HttpServletRequest.class);       
		
		when(ex.invalidateSession(request)).thenReturn(true);		
		assertEquals(true, ex.invalidateSession(request));
	}
	
	
	@Test
	public void submitQuery() throws IOException{
		HttpServletRequest request = mock(HttpServletRequest.class);       
		HttpServletResponse response = mock(HttpServletResponse.class); 
		
		when(ex.submitQuery(request, response)).thenReturn(true);		
		assertEquals(true, ex.submitQuery(request, response));
	}
	
	@Test
	public void registerForm() throws IOException{
		HttpServletRequest request = mock(HttpServletRequest.class);       
		
		when(ex.registerForm(request)).thenReturn(true);		
		assertEquals(true, ex.registerForm(request));
	}
	
	
}
